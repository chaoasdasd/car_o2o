package cn.wolfcode.car.business.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Getter
@Setter
public class Employee {
    /** */
    private Long id;

    /** */
    private String name;    //姓名

    /** */
    private String email;   //邮箱

    /** */
    private Integer age;     // 年龄

    /** */
    private Integer admin;      // 是否超管    【0不是/1是】

    /** */
    private Integer status;     //状态        【0不是/1是】

    /** */
    private Long deptId;        //部门编号      //1 开发部 2财务部 3人事部 4市场部 5总经办 6战略部 7指挥部 8炮灰部

    /** */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT-8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date hiredate;      //聘用日期

    private Deprecated deprecated;
}