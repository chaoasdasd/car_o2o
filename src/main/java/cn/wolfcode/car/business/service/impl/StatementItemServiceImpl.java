package cn.wolfcode.car.business.service.impl;


import cn.wolfcode.car.base.domain.User;
import cn.wolfcode.car.business.domain.ServiceItem;
import cn.wolfcode.car.business.domain.Statement;
import cn.wolfcode.car.business.domain.StatementItem;
import cn.wolfcode.car.business.mapper.StatementItemMapper;
import cn.wolfcode.car.business.mapper.StatementMapper;
import cn.wolfcode.car.business.query.ServiceItemQuery;
import cn.wolfcode.car.business.query.StatementItemQuery;
import cn.wolfcode.car.business.service.IStatementItemService;
import cn.wolfcode.car.common.base.page.TablePageInfo;
import cn.wolfcode.car.common.exception.BusinessException;
import cn.wolfcode.car.common.util.Convert;
import cn.wolfcode.car.shiro.ShiroUtils;
import com.github.pagehelper.PageHelper;
import com.sun.jna.platform.win32.User32Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class StatementItemServiceImpl implements IStatementItemService {

    @Autowired
    private StatementItemMapper statementItemMapper;
    @Autowired
    private StatementMapper statementMapper;


    @Override
    public TablePageInfo<StatementItem> query(StatementItemQuery qo) {
        PageHelper.startPage(qo.getPageNum(), qo.getPageSize());
        return new TablePageInfo<StatementItem>(statementItemMapper.selectForList(qo));
    }


    @Override
    public List<StatementItem> get(Long statementId) {
        return statementItemMapper.selectByPrimaryKey(statementId);
    }


    @Override
    public void save(StatementItem statementItem) {

        statementItemMapper.insert(statementItem);
    }

    @Override
    public void update(StatementItem statementItem) {


    }


    @Override
    public void deleteBatch(String ids) {
        Long[] dictIds = Convert.toLongArray(ids);
        for (Long dictId : dictIds) {
            statementItemMapper.deleteByPrimaryKey(dictId);
        }
    }

    @Override
    public List<StatementItem> list() {
        return statementItemMapper.selectAll();
    }

    @Override
    public void saveItems(List<StatementItem> statementItems) {
        // 先判断状态不能对已支付进行编辑操作
        // 约定 ：删除集合最后一个元素是保存优惠价格 因为遍历不需要
        StatementItem remove = statementItems.remove(statementItems.size() - 1);
        Statement statement = statementMapper.selectByStatementId(remove.getStatementId());
        if (statement.getStatus() != Statement.STATUS_CONSUME) {
            throw new BusinessException("只有消费中的订单可以做保存操作");
        }

        // 总数
        BigDecimal totalQuantity = new BigDecimal("0.00");
        // 总金额
        BigDecimal totalAmount = new BigDecimal("0.00");

        // 做保存之前必须先把之前的明细删除掉 不然会累加
        statementItemMapper.deleteByStatementId(remove.getStatementId());

        // 判断不等于空 集合长度是否大于0 再进行操作
        if (statementItems != null && statementItems.size() > 0) {

            for (StatementItem item : statementItems) {
                statementItemMapper.insert(item);
                // 总金额 等于 总数 * 单价
                totalAmount = totalAmount.add(new BigDecimal(item.getItemQuantity()).multiply(item.getItemPrice()));
                totalQuantity = totalQuantity.add(new BigDecimal(item.getItemQuantity()));
            }
        }

        // 优惠价格不能超过总金额
        if (totalAmount.compareTo(remove.getItemPrice()) < 0) {
            throw new BusinessException("优惠价格不能超过总金额");
        }


        // 设置折扣价
        statement.setDiscountAmount(remove.getItemPrice());
        // 总消费金额
        statement.setTotalAmount(totalAmount);
        // 服务项数量
        statement.setTotalQuantity(totalQuantity);

        // 保存提交  id 总消费金额 服务项数量 折扣价
        statementMapper.updateAmount(statement);

    }


    // 支付操作
    @Override
    public void payStatement(Long statementId) {
        Statement statement = statementMapper.selectByStatementId(statementId);
        // 判断查询出来的对象 是空 或者 不是在消费中就抛异常
        if (statement == null || statement.getStatus() != Statement.STATUS_CONSUME) {
            throw new BusinessException("只有在消费中才能发起支付");
        }

        // 到这成功发起支付 修改状态 存入收款人信息和支付时间
        statement.setStatus(Statement.STATUS_PAID);
        statement.setPayTime(new Date());
        statement.setPayeeId(ShiroUtils.getUserId());
        statementMapper.payStatement(statement);

    }


}
