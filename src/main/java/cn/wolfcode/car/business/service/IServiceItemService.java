package cn.wolfcode.car.business.service;


import cn.wolfcode.car.business.domain.ServiceItem;
import cn.wolfcode.car.business.query.ServiceItemQuery;
import cn.wolfcode.car.common.base.page.TablePageInfo;

import java.util.List;

/**
 * 岗位服务接口
 */
public interface IServiceItemService {

    /**
     * 分页
     * @param qo
     * @return
     */
    TablePageInfo<ServiceItem> query(ServiceItemQuery qo);


    /**
     * 查单个
     * @param id
     * @return
     */
    ServiceItem get(Long id);


    /**
     * 保存
     * @param serviceItem
     */
    void save(ServiceItem serviceItem);

  
    /**
     * 更新
     * @param serviceItem
     */
    void update(ServiceItem serviceItem);

    /**
     *  批量删除
     * @param ids
     */
    void deleteBatch(String ids);

    /**
     * 查询全部
     * @return
     */
    List<ServiceItem> list();


    void saleOff(Long id);

    void saleOn(Long id);

    /**
     * 保存审核
     * @param id            要审核的服务项id
     * @param bpmnInfoId    流程定义id
     * @param director      店长的id
     * @param finance      财务的id
     * @param info          信息
     */
    void startAudit(Long id, Long bpmnInfoId, Long director, Long finance, String info);

    /**
     * 修改服务单项为初始化状态
     * @param serviceItemId
     * @param auditstatusInit
     */
    void changAuditStatus(Long serviceItemId, Integer auditstatusInit);
}
