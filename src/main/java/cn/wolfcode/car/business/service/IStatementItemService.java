package cn.wolfcode.car.business.service;


import cn.wolfcode.car.business.domain.StatementItem;
import cn.wolfcode.car.business.query.ServiceItemQuery;
import cn.wolfcode.car.business.query.StatementItemQuery;
import cn.wolfcode.car.common.base.page.TablePageInfo;

import java.util.List;

/**
 * 岗位服务接口
 */
public interface IStatementItemService {

    /**
     * 分页
     * @param qo
     * @return
     */
    TablePageInfo<StatementItem> query(StatementItemQuery qo);


    /**
     * 查单个
     * @param id
     * @return
     */
    List<StatementItem> get(Long id);


    /**
     * 保存
     * @param statementItem
     */
    void save(StatementItem statementItem);

  
    /**
     * 更新
     * @param statementItem
     */
    void update(StatementItem statementItem);

    /**
     *  批量删除
     * @param ids
     */
    void deleteBatch(String ids);

    /**
     * 查询全部
     * @return
     */
    List<StatementItem> list();

    /**
     * 保存结算明细单
     * @param statementItems
     */
    void saveItems(List<StatementItem> statementItems);

    /**
     * 提交支付操作
     * @param statementId 要提交的id
     */
    void payStatement(Long statementId);

}
