package cn.wolfcode.car.business.service;


import cn.wolfcode.car.business.domain.Department;
import cn.wolfcode.car.business.query.DepartmentQuery;
import cn.wolfcode.car.common.base.page.TablePageInfo;

import java.util.List;

/**
 * 岗位服务接口
 */
public interface IDepartmentService {

    /**
     * 分页
     * @param qo
     * @return
     */
    TablePageInfo<Department> query(DepartmentQuery qo);



}
