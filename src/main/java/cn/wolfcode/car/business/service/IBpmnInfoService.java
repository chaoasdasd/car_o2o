package cn.wolfcode.car.business.service;


import cn.wolfcode.car.business.domain.BpmnInfo;
import cn.wolfcode.car.business.query.BpmnInfoQuery;
import cn.wolfcode.car.common.base.page.TablePageInfo;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

/**
 * 流程定义服务接口
 */
public interface IBpmnInfoService {

    /**
     * 分页
     * @param qo
     * @return
     */
    TablePageInfo<BpmnInfo> query(BpmnInfoQuery qo);


    /**
     * 查单个
     * @param id
     * @return
     */
    BpmnInfo get(Long id);


    /**
     * 保存
     * @param bpmnInfo
     */
    void save(BpmnInfo bpmnInfo);

  
    /**
     * 更新
     * @param bpmnInfo
     */
    void update(BpmnInfo bpmnInfo);

    /**
     *  批量删除
     * @param ids
     */
    void deleteBatch(String ids);

    /**
     * 查询全部
     * @return
     */
    List<BpmnInfo> list();


    void deploy(String bpmnInfoPath, String bpmnType, String info) throws FileNotFoundException;

    /**
     * 删除流程定义
     * @param id
     */
    void delete(Long id);

    /**
     * 从activiti7中查询资源文件
     * @param deploymentId
     * @param type
     * @return
     */
    InputStream readResource(String deploymentId, String type);

    /**
     * 根据类型查询审核流程对象
     * @param type
     * @return
     */
    BpmnInfo queryByType(String type);
}
