package cn.wolfcode.car.business.service.impl;


import cn.wolfcode.car.business.domain.Appointment;
import cn.wolfcode.car.business.domain.Statement;
import cn.wolfcode.car.business.mapper.AppointmentMapper;
import cn.wolfcode.car.business.query.AppointmentQuery;
import cn.wolfcode.car.business.service.IAppointmentService;
import cn.wolfcode.car.common.base.page.TablePageInfo;
import cn.wolfcode.car.common.exception.BusinessException;
import cn.wolfcode.car.common.util.Convert;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class AppointmentServiceImpl implements IAppointmentService {

    @Autowired
    private AppointmentMapper appointmentMapper;

    @Autowired
    private StatementServiceImpl statementService;

    @Override
    public TablePageInfo<Appointment> query(AppointmentQuery qo) {
        PageHelper.startPage(qo.getPageNum(), qo.getPageSize());
        return new TablePageInfo<Appointment>(appointmentMapper.selectForList(qo));
    }


    @Override
    public Appointment get(Long id) {
        return appointmentMapper.selectByPrimaryKey(id);
    }

    @Override
    public void save(Appointment appointment) {
        // 设置创建时间
        appointment.setCreateTime(new Date());
        // 把新增数据设置成预约中状态 防止数据被改动过
        appointment.setStatus(Appointment.STATUS_APPOINTMENT);

        appointmentMapper.insert(appointment);
    }

    @Override
    public void update(Appointment appointment) {
        Appointment appointment1 = appointmentMapper.get(appointment.getId());
        // 如果状态不是预约中 不能进行编辑操作
        if(appointment1 == null || !Appointment.STATUS_APPOINTMENT.equals(appointment1.getStatus())){
            throw new BusinessException("不是预约中不能进行编辑操作");
        }

        // 设置数据 把想设置进去数据库的通过查询出来的对象
        appointment1.setCustomerName(appointment.getCustomerName());
        appointment1.setCustomerPhone(appointment.getCustomerPhone());
        appointment1.setAppointmentTime(appointment.getAppointmentTime());
        appointment1.setLicensePlate(appointment.getLicensePlate());
        appointment1.setCarSeries(appointment.getCarSeries());
        appointment1.setServiceType(appointment.getServiceType());
        appointment1.setInfo(appointment.getInfo());

        appointmentMapper.updateByPrimaryKey(appointment1);
    }


    @Override
    public void deleteBatch(String ids) {
        Long[] dictIds = Convert.toLongArray(ids);
        for (Long dictId : dictIds) {
            appointmentMapper.deleteByPrimaryKey(dictId);
        }
    }

    @Override
    public List<Appointment> list() {
        return appointmentMapper.selectAll();
    }

    @Override
    public void cancel(Long id) {
        //通过id查询数据
        Appointment appointment = get(id);
        // 是空抛异常 判断是不是预约状态才能取消预约 否则抛出异常
        if(appointment == null || !Appointment.STATUS_APPOINTMENT.equals(appointment.getStatus())){
            throw new BusinessException("只有预约状态才能取消");
        }

        // 修改状态 取消预约
        appointmentMapper.cancelById(id,Appointment.STATUS_CANCEL);

    }

    @Override
    public void reach(Long id) {
        //通过id查询数据
        Appointment appointment = get(id);
        // 是空抛异常 判断是不是预约状态才能到店操作 否则抛出异常
        if(appointment == null || !Appointment.STATUS_APPOINTMENT.equals(appointment.getStatus())){
            throw new BusinessException("只有预约状态才能点击到店");
        }

        // 修改状态 到店
        appointmentMapper.cancelById(id,Appointment.STATUS_ARRIVAL);

    }

    //预约单 queryAppointmentId
    @Override
    public Long generateStatement(Long id) {

        Appointment ap = appointmentMapper.selectByPrimaryKey(id);
        Statement statement = null; //statementService.getByAppointmentId(id);
        // 如果结算单已经生成 查询结算单 返回id
        if (ap.getStatus().equals(Appointment.STATUS_SETTLE)){
            statement  = statementService.queryAppointmentId(id);
        }else {
            // 如果状态为已到店 创建结算单 返回id
            statement  =new Statement();
            //要吧结算单表中的设置养修预约 id 值设置进去
            statement.setAppointmentId(ap.getId());
            statement.setCustomerName(ap.getCustomerName());
            statement.setCustomerPhone(ap.getCustomerPhone());
            statement.setActualArrivalTime(ap.getActualArrivalTime());
            statement.setLicensePlate(ap.getLicensePlate());
            statement.setCarSeries(ap.getCarSeries());
            statement.setServiceType(ap.getServiceType().longValue());
            statement.setInfo(ap.getInfo());
            statement.setStatus(Statement.STATUS_CONSUME);
            statementService.save(statement);
            //改变养修预约中的 装态改为 结算单生成
            appointmentMapper.cancelById(ap.getId(),Appointment.STATUS_SETTLE);
        }
        return statement.getId();

    }
}
