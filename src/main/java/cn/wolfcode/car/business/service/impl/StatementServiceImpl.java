package cn.wolfcode.car.business.service.impl;


import cn.wolfcode.car.business.domain.Appointment;
import cn.wolfcode.car.business.domain.Statement;
import cn.wolfcode.car.business.mapper.AppointmentMapper;
import cn.wolfcode.car.business.mapper.StatementItemMapper;
import cn.wolfcode.car.business.mapper.StatementMapper;
import cn.wolfcode.car.business.query.StatementQuery;
import cn.wolfcode.car.business.service.IStatementService;
import cn.wolfcode.car.common.base.page.TablePageInfo;
import cn.wolfcode.car.common.exception.BusinessException;
import cn.wolfcode.car.common.util.Convert;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class StatementServiceImpl implements IStatementService {

    @Autowired
    private StatementMapper statementMapper;

    @Autowired
    private StatementServiceImpl statementItemService;

    @Autowired
    private StatementItemMapper statementItemMapper;

    @Autowired
    private AppointmentMapper appointmentMapper;

    @Override
    public TablePageInfo<Statement> query(StatementQuery qo) {
        PageHelper.startPage(qo.getPageNum(), qo.getPageSize());
        return new TablePageInfo<Statement>(statementMapper.selectForList(qo));
    }


    @Override
    public Statement get(Long id) {
        return statementMapper.selectByPrimaryKey(id);
    }

    @Override
    public void save(Statement statement) {
            // 添加养修结算单创建时间
            statement.setCreateTime(new Date());
            // 设置成消费中
            statement.setStatus(Statement.STATUS_CONSUME);
            // 设置到店时间
            statement.setActualArrivalTime(new Date());
            statementMapper.insert(statement);
    }

    @Override
    public void update(Statement statement) {
        // 通过id查询出对象
        Statement st = this.get(statement.getId());
        // 如果查出来的对象等于空 或者 不是消费中状态 不允许修改 抛异常提示
        if(st == null || !st.getStatus().equals(Statement.STATUS_CONSUME)){
            throw new BusinessException("不是消费中状态 不能修改");
        }

        statementMapper.updateStatement(statement);
    }


    @Override
    public void deleteBatch(String ids) {
        Long[] dictIds = Convert.toLongArray(ids);
        for (Long dictId : dictIds) {
            statementMapper.deleteByPrimaryKey(dictId);
        }
    }

    @Override
    public List<Statement> list() {
        return statementMapper.selectAll();
    }

    @Override //查询预约ID
    public Statement queryAppointmentId(Long id) {
       return statementMapper.selectByAppointmentId(id);

    }

    @Override
    public void delete(Long id) {
        // 关联预约单修改状态 结算单id 预约单id 改状态
        Statement statement = statementMapper.selectByPrimaryKey(id);
        // 结算单删除
        statementMapper.deleteByPrimaryKey(id);

        // 结算单明细删除
        statementItemMapper.deleteByStatementId(id);

        Long appointmentId = statement.getAppointmentId();
        if(appointmentId != null){
            //更新状态
            appointmentMapper.updateStatus(appointmentId, Appointment.STATUS_ARRIVAL);
        }
    }



}
