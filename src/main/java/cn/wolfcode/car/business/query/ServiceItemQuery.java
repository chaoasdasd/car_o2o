package cn.wolfcode.car.business.query;

import cn.wolfcode.car.common.base.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceItemQuery extends QueryObject {
    private String name;                //服务项名称
    private Integer carPackage;         // 是否套餐
    private Integer serviceCatalog;     // 服务分类
    private Integer auditStatus;        // 审核状态
    private Integer saleStatus;         // 上架状态


}
