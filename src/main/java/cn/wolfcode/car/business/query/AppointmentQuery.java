package cn.wolfcode.car.business.query;

import cn.wolfcode.car.common.base.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppointmentQuery extends QueryObject {
    private String customerName;                    //客户姓名

    private String customerPhone;                   //客户联系方式

    private Integer status;    //状态【预约中0/已到店1/用户取消2/超时取消3】
}
