package cn.wolfcode.car.business.query;

import cn.wolfcode.car.common.base.query.QueryObject;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Calendar;
import java.util.Date;

@Getter
@Setter
public class EmployeeQuery extends QueryObject {

    private String keywords; // 关键字
    private Integer admin;  //是否超管
    private Integer status; //状态 是否冻结
    private  Long deptId ; //所在部门


    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date beginTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endTime;


    // 指定时间最后一秒
    public Date getEndTime(){
        if(endTime == null){
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endTime);

        calendar.set(Calendar.HOUR_OF_DAY,23);
        calendar.set(Calendar.MINUTE,59);
        calendar.set(Calendar.SECOND,59);

        return calendar.getTime();
    }

}
