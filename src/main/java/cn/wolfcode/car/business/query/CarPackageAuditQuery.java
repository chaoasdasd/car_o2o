package cn.wolfcode.car.business.query;

import cn.wolfcode.car.common.base.query.QueryObject;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Calendar;
import java.util.Date;

@Getter
@Setter
public class CarPackageAuditQuery extends QueryObject {
    // 1>当前节点审核人必须是当前登录的用户

    //2>审核记录的必须是审核中状态
    // 审核中状态
    private Integer state;

    // 当前登录用户的id 当前审核人
    private Long auditorId;

    private String auditRecord; //审核记录

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date beginTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endTime;


    // 指定时间最后一秒
    public Date getEndTime(){
        if(endTime == null){
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endTime);

        calendar.set(Calendar.HOUR_OF_DAY,23);
        calendar.set(Calendar.MINUTE,59);
        calendar.set(Calendar.SECOND,59);

        return calendar.getTime();
    }
}
