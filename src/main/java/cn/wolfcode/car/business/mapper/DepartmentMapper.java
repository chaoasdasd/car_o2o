package cn.wolfcode.car.business.mapper;

import cn.wolfcode.car.business.domain.Department;
import cn.wolfcode.car.business.query.DepartmentQuery;

import java.util.List;

public interface DepartmentMapper {


    Department selectByPrimaryKey(Long id);

    List<Department> selectAll();


    List<Department> selectForList(DepartmentQuery qo);
}