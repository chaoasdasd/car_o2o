package cn.wolfcode.car.business.web.controller;


import cn.wolfcode.car.business.domain.Department;
import cn.wolfcode.car.business.query.DepartmentQuery;
import cn.wolfcode.car.business.service.IDepartmentService;
import cn.wolfcode.car.common.base.page.TablePageInfo;
import cn.wolfcode.car.common.web.AjaxResult;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 养修预约控制器
 */
@Controller
@RequestMapping("business/department")
public class DepartmentController {
    //模板前缀
    private static final String prefix = "business/department/";

    @Autowired
    private IDepartmentService departmentService;

    //页面------------------------------------------------------------
    //列表
    @RequiresPermissions("system:department:view")
    @RequestMapping("/listPage")
    public String listPage(){
        return prefix + "list";
    }



    //数据-----------------------------------------------------------
    //列表
    @RequiresPermissions("system:department:list")
    @RequestMapping("/query")
    @ResponseBody
    public TablePageInfo<Department> query(DepartmentQuery qo){
        return departmentService.query(qo);
    }


}
