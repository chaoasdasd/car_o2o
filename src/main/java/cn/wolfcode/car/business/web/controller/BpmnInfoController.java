package cn.wolfcode.car.business.web.controller;


import cn.wolfcode.car.business.domain.BpmnInfo;
import cn.wolfcode.car.business.query.BpmnInfoQuery;
import cn.wolfcode.car.business.service.IBpmnInfoService;
import cn.wolfcode.car.common.base.page.TablePageInfo;
import cn.wolfcode.car.common.config.SystemConfig;
import cn.wolfcode.car.common.util.file.FileUploadUtils;
import cn.wolfcode.car.common.web.AjaxResult;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletConfig;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.http.HttpResponse;

/**
 * 流程定义控制器
 */
@Controller
@RequestMapping("business/bpmnInfo")
public class BpmnInfoController {
    //模板前缀
    private static final String prefix = "business/bpmnInfo/";

    @Autowired
    private IBpmnInfoService bpmnInfoService;

    //页面------------------------------------------------------------
    //列表
    @RequiresPermissions("system:bpmnInfo:view")
    @RequestMapping("/listPage")
    public String listPage() {
        return prefix + "list";
    }

    @RequiresPermissions("system:bpmnInfo:add")
    @RequestMapping("/addPage")
    public String addPage() {
        return prefix + "add";
    }


    @RequiresPermissions("system:bpmnInfo:edit")
    @RequestMapping("/editPage")
    public String editPage(Long id, Model model) {
        model.addAttribute("bpmnInfo", bpmnInfoService.get(id));
        return prefix + "edit";
    }
    @RequiresPermissions("system:bpmnInfo:deployPage")
    @RequestMapping("/deployPage")
    public String deployPage(Long id, Model model) {
        model.addAttribute("bpmnInfo", bpmnInfoService.get(id));
        return prefix + "deployPage";
    }


    //数据-----------------------------------------------------------
    //列表
    @RequiresPermissions("system:bpmnInfo:list")
    @RequestMapping("/query")
    @ResponseBody
    public TablePageInfo<BpmnInfo> query(BpmnInfoQuery qo) {
        return bpmnInfoService.query(qo);
    }


    //新增
    @RequiresPermissions("system:bpmnInfo:add")
    @RequestMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BpmnInfo bpmnInfo) {
        bpmnInfoService.save(bpmnInfo);
        return AjaxResult.success();
    }

    //编辑
    @RequiresPermissions("system:bpmnInfo:edit")
    @RequestMapping("/edit")
    @ResponseBody
    public AjaxResult edit(BpmnInfo bpmnInfo) {
        bpmnInfoService.update(bpmnInfo);
        return AjaxResult.success();
    }
    //查看资源文件 xml格式 png格式
    @RequiresPermissions("system:bpmnInfo:readResource")
    @RequestMapping("/readResource")
    public void readResource(String deploymentId, String type, HttpServletResponse response) throws IOException {

        // 从activiti7 查询出xml文件跟png文件，通过input流方式返回
       InputStream input = bpmnInfoService.readResource(deploymentId,type);
        // 将图片/文件通过output流方式输出到浏览器
        IOUtils.copy(input,response.getOutputStream());
    }


    //删除
    @RequiresPermissions("system:bpmnInfo:remove")
    @RequestMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        bpmnInfoService.deleteBatch(ids);
        return AjaxResult.success();
    }
    //实现流程定义文件删除**
    @RequiresPermissions("system:bpmnInfo:delete")
    @RequestMapping("/delete")
    @ResponseBody
    public AjaxResult delete(Long id) {
        bpmnInfoService.delete(id);
        return AjaxResult.success();
    }


    //部署
   // @RequiresPermissions("business/bpmnInfo:deploy")
    @RequestMapping("/deploy")
    @ResponseBody
    public AjaxResult deploy(String bpmnInfoPath,String bpmnType,String info) throws FileNotFoundException {

        bpmnInfoService.deploy(bpmnInfoPath,bpmnType,info);
        return AjaxResult.success();
    }

    //上传文件
   // @RequiresPermissions("system:bpmnInfo:upload")
    @RequestMapping("/upload")
    @ResponseBody
    public AjaxResult upload(MultipartFile file) throws IOException {


        if(file != null && file.getSize() > 0){
            String originalFilename = file.getOriginalFilename();
            String extName = FilenameUtils.getExtension(originalFilename);

            // 判断是否.bpmn文件结尾
            if("bpmn".equalsIgnoreCase(extName)){
                String uploadPath = FileUploadUtils.upload(SystemConfig.getUploadPath(), file);
                return AjaxResult.success("上传成功",uploadPath);
            }else {
                return AjaxResult.error("流程定义文件仅支持 bpmn格式！");
            }

        }else {
            return AjaxResult.error("不允许上传空文件");
        }

    }

}
