package cn.wolfcode.car.business.web.controller;


import cn.wolfcode.car.base.service.impl.UserServiceImpl;
import cn.wolfcode.car.business.domain.BpmnInfo;
import cn.wolfcode.car.business.domain.ServiceItem;
import cn.wolfcode.car.business.domain.Statement;
import cn.wolfcode.car.business.domain.StatementItem;
import cn.wolfcode.car.business.mapper.StatementMapper;
import cn.wolfcode.car.business.query.ServiceItemQuery;
import cn.wolfcode.car.business.query.StatementItemQuery;
import cn.wolfcode.car.business.service.IAppointmentService;
import cn.wolfcode.car.business.service.IServiceItemService;
import cn.wolfcode.car.business.service.IStatementItemService;
import cn.wolfcode.car.business.service.IStatementService;
import cn.wolfcode.car.business.service.impl.BpmnInfoServiceImpl;
import cn.wolfcode.car.common.base.page.TablePageInfo;
import cn.wolfcode.car.common.web.AjaxResult;
import cn.wolfcode.car.shiro.ShiroUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 养修预约控制器
 */
@Controller
@RequestMapping("business/statementItem")
public class StatementItemController {
    //模板前缀
    private static final String prefix = "business/statementItem/";

    @Autowired
    private IStatementItemService statementItemService;

    @Autowired
    private IStatementService statementService;

    @Autowired
    private IAppointmentService appointmentService;

    @Autowired
    private IServiceItemService serviceItemService;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private BpmnInfoServiceImpl bpmnInfoService;

    //页面------------------------------------------------------------
    //列表
    @RequiresPermissions("system:statementItem:view")
    @RequestMapping("/listPage")
    public String listPage() {
        return prefix + "list";
    }

    //数据-----------------------------------------------------------
    //列表
    @RequiresPermissions("system:statementItem:generateStatement")
    @RequestMapping("/generateStatement")
    public String generateStatement(Long id){
        // 如果状态为已经到店 创建结算单 返回id
        // 如果状态为：结算单生成 就查询结算单 返回id
        // 结算单id返回出去
        Long statementId = appointmentService.generateStatement(id);

        return "redirect:/business/statementItem/itemDetail?statementId=" + statementId;
    }


    @RequiresPermissions("system:statementItem:list")
    @RequestMapping("/query")
    @ResponseBody
    public TablePageInfo<StatementItem> query(StatementItemQuery qo) {
        return statementItemService.query(qo);
    }

    @RequiresPermissions("system:statementItem:saveItems")
    @RequestMapping("/saveItems")
    @ResponseBody
    public AjaxResult saveItems(@RequestBody List<StatementItem> statementItems) {
        // 页面提交服务结算单明细
        statementItemService.saveItems(statementItems);

        return  AjaxResult.success();
    }

    @RequiresPermissions("system:statementItem:payStatement")
    @RequestMapping("/payStatement")
    @ResponseBody
    public AjaxResult payStatement( Long statementId) {
        // 页面发起支付请求
        statementItemService.payStatement(statementId);

        return  AjaxResult.success();
    }


    //项目详情
    @RequiresPermissions("system:statementItem:itemDetail")
    @RequestMapping("/itemDetail")
    public String itemDetail(Model model, Long statementId) {
        // 拿到结算单id查出结算单 存入model中
        Statement statement = statementService.get(statementId);
        model.addAttribute("statement",statement);


        //判断是否在消费中 消费中可以编辑 已经支付的只能查看
        if(statement.getStatus() == Statement.STATUS_CONSUME){
            return prefix + "editDetail";
        }else {
            statement.setPayee(ShiroUtils.getUser());
            return prefix + "showDetail";
        }
    }

}
