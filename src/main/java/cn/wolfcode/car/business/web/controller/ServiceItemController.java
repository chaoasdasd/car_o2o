package cn.wolfcode.car.business.web.controller;


import cn.wolfcode.car.base.domain.User;
import cn.wolfcode.car.base.service.impl.UserServiceImpl;
import cn.wolfcode.car.business.domain.BpmnInfo;
import cn.wolfcode.car.business.domain.ServiceItem;
import cn.wolfcode.car.business.query.ServiceItemQuery;
import cn.wolfcode.car.business.service.IServiceItemService;
import cn.wolfcode.car.business.service.impl.BpmnInfoServiceImpl;
import cn.wolfcode.car.common.base.page.TablePageInfo;
import cn.wolfcode.car.common.web.AjaxResult;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 养修预约控制器
 */
@Controller
@RequestMapping("business/serviceItem")
public class ServiceItemController {
    //模板前缀
    private static final String prefix = "business/serviceItem/";

    @Autowired
    private IServiceItemService serviceItemService;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private BpmnInfoServiceImpl bpmnInfoService;

    //页面------------------------------------------------------------
    //列表
    @RequiresPermissions("system:serviceItem:view")
    @RequestMapping("/listPage")
    public String listPage(){
        return prefix + "list";
    }

    @RequiresPermissions("system:serviceItem:add")
    @RequestMapping("/addPage")
    public String addPage(){
        return prefix + "add";
    }


    @RequiresPermissions("system:serviceItem:edit")
    @RequestMapping("/editPage")
    public String editPage(Long id, Model model){
        model.addAttribute("serviceItem", serviceItemService.get(id));
        return prefix + "edit";
    }


    @RequestMapping("/auditPage")
    public String auditPage(Model model, Long id){
        model.addAttribute("serviceItem", serviceItemService.get(id));
        // 业务上约定 一个盛和流程对应唯一一个流程图 -- 就是说 bpmn_type 列是唯一的
        // car_package---套餐审核
        //流程定义我呢见-bpmnInfo
        BpmnInfo bpmnInfo = bpmnInfoService.queryByType("car_package");
        model.addAttribute("bpmnInfo", bpmnInfo);


        //店长-- 集合-- 角色-- 查询该角色所有用户集合
        List<User> directors = userService.queryByRoleKey("shopOwner");
        model.addAttribute("directors", directors);
        //财务集合-- 角色
        List<User> finances = userService.queryByRoleKey("financial");
        model.addAttribute("finances", finances);

        model.addAttribute("serviceItem",serviceItemService.get(id));
        return prefix + "auditPage";
    }


    //数据-----------------------------------------------------------
    //列表
    @RequiresPermissions("system:serviceItem:list")
    @RequestMapping("/query")
    @ResponseBody
    public TablePageInfo<ServiceItem> query(ServiceItemQuery qo){
        return serviceItemService.query(qo);
    }

    //养修服务项
    @RequiresPermissions("system:serviceItem:selectAllSaleOnList")
    @RequestMapping("/selectAllSaleOnList")
    @ResponseBody
    public TablePageInfo<ServiceItem> selectAllSaleOnList(ServiceItemQuery qo){
        // 设置只查询上架状态的商品
        qo.setSaleStatus(ServiceItem.SALESTATUS_ON);

        return serviceItemService.query(qo);
    }



    //新增
    @RequiresPermissions("system:serviceItem:add")
    @RequestMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ServiceItem serviceItem){
        serviceItemService.save(serviceItem);
        return AjaxResult.success();
    }

    //编辑
    @RequiresPermissions("system:serviceItem:edit")
    @RequestMapping("/edit")
    @ResponseBody
    public AjaxResult edit(ServiceItem serviceItem){
        serviceItemService.update(serviceItem);
        return AjaxResult.success();
    }

    //删除
    @RequiresPermissions("system:serviceItem:remove")
    @RequestMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids){
        serviceItemService.deleteBatch(ids);
        return AjaxResult.success();
    }

    //下架
   // @RequiresPermissions("system:serviceItem:saleOff")
    @RequestMapping("/saleOff")
    @ResponseBody
    public AjaxResult saleOff(Long id){
        serviceItemService.saleOff(id);
        return AjaxResult.success();
    }

    //上架
   // @RequiresPermissions("system:serviceItem:saleOn")
    @RequestMapping("/saleOn")
    @ResponseBody
    public AjaxResult saleOn(Long id){
        serviceItemService.saleOn(id);
        return AjaxResult.success();
    }

    //开始审计
    @RequestMapping("/startAudit")
    @ResponseBody
    public AjaxResult startAudit(Long id,Long bpmnInfoId,Long director,Long finance,String info){
        serviceItemService.startAudit(id,bpmnInfoId,director,finance,info);
        return AjaxResult.success();
    }


}
